const express = require("express");
const app = express();
const { config } = require("./config/");

app.get("/", function(req, res) {
  return res.send("Hello World");
});

app.listen(config.port);
